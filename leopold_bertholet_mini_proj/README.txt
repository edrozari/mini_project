OTA_mini_project contains the schematic, layout, symbol, and extracted OTA.
tb_OTA_mini_project contains the testbench schematic, a config file, and output_plot. Open output_plot to plot both the schematic output (Vout) and extracted output (Vout2).
