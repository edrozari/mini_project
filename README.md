You will submit your cadence design files for the Mini_Projects here. The specifications of the project are in canvas ELMS. You will submit your report in Canvas. 

The submission should follow the following format. The top cell containing the full design should be called Top_cell:
Top_level_folder:
   Your name(eg: Sahil Shah)
        -Top_library
          -Cells (eg: Top_cell)
               -Schematic
               -Layout

The top cell should pass both LVS and DRC. 
