Matthew Chen - ENEE408D Mini-Project
This mini-project submission contains four  folders inside the "Matthew_Chen" folder: "OTA", "OTAPowerTestbench", "Testbench", and "TBstates".

OTA: This folder contains the symbol, layout, schematic, and extracted OTA. The schematic matches the miniproject rubric, and the layout/extracted view pass LVS and DRC checks.

TBstates: This folder contains the ADE L testing states with correct analysis type/outputs as shown in the report. To load them in properly, copy both the "OTAPowerTestbench" and "Testbench" folders from inside "TBstates" into "/home/enee408d_student/.artist_states/408D" directory ( or "~/.artist_states/408D"), which is where ADE L reads the state files from. This should allow you to not have to manually set up anything when running the included testbenches.

OTAPowerTestbench: Contains a schematic for determining power consumption of the OTA. To run, launch ADE L and load in the state "transient", which is included in TBstates. From here, you should be able to netlist and run, and see a plot of current drawn by the OTA.

Testbench: Contains a schematic and config file to test the OTA - displays a gain vs. frequency graph. To use, open the config and launch ADE L, and set the simulator type to spectre. Then, follow similar steps to OTAPowerTestbench - load in the state "ac2", which is included in TBstates. From here, you should be able to netlist and run, and see a plot of gain vs. frequency for the schematic and extracted OTA.
