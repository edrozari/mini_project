Schematic Simulation Instructions:
1. Go to testbench schematic under OTA_TB
2. Launch ADE L within the schematic
3. Setup model libraries
4. Copy design variables from cellview, set vdd = 5V, and vss = -5V
5. Add ac analysis with a frequency sweep from 1Hz to 1GHz with automatic sweep type
6. Add outputs to be plotted on design V+, V-, and Vout
7. Also add dB20(VF("/Vout")) from calculator to see the plots in dB

Layout Simulation Instructions:
1. Open the config file and go to heirarchy editor
2. Launch ADE L from within the config file
3. Setup the same as the schematic simulation, but add dB20(VF("/Vout2")) and include output to be plotted Vout2 to see the plots from the schematic with the parasitics included
