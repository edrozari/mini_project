Simulation Instructions:
1. Open Mini_Project_Testbench config 
2. Launch ADE L from within the config file
3. Setup simulator spectre
4. Setup model libraries V3BM
5. Add ac analysis with a frequency sweep from 1Hz to 1GHz with logarithmec sweep type (100 points per decade)
6. Add outputs to be plotted on design V+, V-, Vout, V2+, V2-, V2out
7. Netlist and run
8. Go to caclulater and add dB20 to Vout and V2out
